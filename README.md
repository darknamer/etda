# การพัฒนาระบบใบเสร็จอิเล็กทรอนิกส์ ทาง สพธอ และกรมบัญชีกลาง ได้ร่วมกันจัดทำมาตรฐานข้อความทางอิเล็กอทรอนิกส์สำหรับใบเสร็จรับเงิน (EDTA)

## ประกาศข้อเสนอแนะมาตรฐานฯ ว่าด้วยข้อความอิเล็กทรอนิกส์สำหรับใบเสร็จรับเงินภาครัฐ เลขที่ ขมธอ. 22-2563

ประกาศแล้ว !! ETDA Recommendation: Message Standard for Government Receipt กำหนดโครงสร้างข้อมูลของข้อความอิเล็กทรอนิกส์ในรูปแบบ XML (Extensible Markup Language) ให้สอดคล้องกับมาตรฐานสากล และเพื่อให้หน่วยงานของรัฐนำไปใช้เป็นมาตรฐานประกอบการจัดทำข้อมูลในใบเสร็จรับเงินได้อย่างมีประสิทธิภาพ

ประกาศเมื่อวันที่ 31 มีนาคม พ.ศ. 2563 โดยข้อเสนอแนะมาตรฐานฯ ฉบับนี้ กำหนดโครงสร้างข้อมูลของข้อความอิเล็กทรอนิกส์ในรูปแบบ XML (Extensible Markup Language) ให้สอดคล้องกับมาตรฐานสากลของ the United Nations Centre for Trade Facilitation and Electronic Business (UN/CEFACT) และเพื่อให้หน่วยงานของรัฐนำไปใช้เป็นมาตรฐานประกอบการจัดทำข้อมูลในใบเสร็จรับเงินได้อย่างมีประสิทธิภาพ

### เอกสาร

[Download](https://www.etda.or.th/getattachment/3a73adaa-115f-4267-9218-35de4d4e5e6d/%E0%B8%82%E0%B8%AD%E0%B9%80%E0%B8%AA%E0%B8%99%E0%B8%AD%E0%B9%81%E0%B8%99%E0%B8%B0%E0%B8%A1%E0%B8%B2%E0%B8%95%E0%B8%A3%E0%B8%90%E0%B8%B2%E0%B8%99%E0%B8%AF-%E0%B8%82%E0%B8%AD%E0%B8%84%E0%B8%A7%E0%B8%B2%E0%B8%A1%E0%B8%AD%E0%B9%80%E0%B8%A5%E0%B8%81%E0%B8%97%E0%B8%A3%E0%B8%AD%E0%B8%99%E0%B8%81%E0%B8%AA%E0%B8%AA%E0%B8%B3%E0%B8%AB%E0%B8%A3%E0%B8%9A%E0%B9%83%E0%B8%9A%E0%B9%80%E0%B8%AA%E0%B8%A3%E0%B8%88%E0%B8%A3%E0%B8%9A%E0%B9%80%E0%B8%87%E0%B8%99.aspx) เอกสารข้อเสนอในการพัฒนาระบบ

[Download](https://schemas.teda.th/teda/teda-objects/common/government-receipt/-/raw/master/Specification-GovernmentReceipt-V2.0.0.xlsx?inline=false) เอกสารมาตรฐานการพัฒนา


### ข้อมูลมาตราฐาน

https://schemas.teda.th/teda/teda-objects/common/government-receipt

### ระบบทดสอบการยืนยันความถูกต้อง

https://validation.teda.th/webportal/v2/#/validate
