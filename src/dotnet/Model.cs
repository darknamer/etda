/* 
 Licensed under the Apache License, Version 2.0

 http://www.apache.org/licenses/LICENSE-2.0
 */
using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Xml2CSharp
{
    [XmlRoot(ElementName = "ID", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class ID
    {
        [XmlAttribute(AttributeName = "schemeAgencyName")]
        public string SchemeAgencyName { get; set; }
        [XmlAttribute(AttributeName = "schemeVersionID")]
        public string SchemeVersionID { get; set; }
        [XmlText]
        public string Text { get; set; }
        [XmlAttribute(AttributeName = "schemeID")]
        public string SchemeID { get; set; }
    }

    [XmlRoot(ElementName = "GuidelineSpecifiedDocumentContextParameter", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class GuidelineSpecifiedDocumentContextParameter
    {
        [XmlElement(ElementName = "ID", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public ID ID { get; set; }
    }

    [XmlRoot(ElementName = "ExchangedDocumentContext", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt:2")]
    public class ExchangedDocumentContext
    {
        [XmlElement(ElementName = "GuidelineSpecifiedDocumentContextParameter", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public GuidelineSpecifiedDocumentContextParameter GuidelineSpecifiedDocumentContextParameter { get; set; }
    }

    [XmlRoot(ElementName = "IncludedNote", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class IncludedNote
    {
        [XmlElement(ElementName = "Subject", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string Subject { get; set; }
        [XmlElement(ElementName = "Content", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string Content { get; set; }
    }

    [XmlRoot(ElementName = "ExchangedDocument", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt:2")]
    public class ExchangedDocument
    {
        [XmlElement(ElementName = "ID", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string ID { get; set; }
        [XmlElement(ElementName = "Name", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string Name { get; set; }
        [XmlElement(ElementName = "TypeCode", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string TypeCode { get; set; }
        [XmlElement(ElementName = "IssueDateTime", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string IssueDateTime { get; set; }
        [XmlElement(ElementName = "Purpose", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string Purpose { get; set; }
        [XmlElement(ElementName = "PurposeCode", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string PurposeCode { get; set; }
        [XmlElement(ElementName = "CreationDateTime", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string CreationDateTime { get; set; }
        [XmlElement(ElementName = "IncludedNote", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public IncludedNote IncludedNote { get; set; }
    }

    [XmlRoot(ElementName = "AssociatedDocumentLineDocument", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class AssociatedDocumentLineDocument
    {
        [XmlElement(ElementName = "LineID", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string LineID { get; set; }
    }

    [XmlRoot(ElementName = "InformationNote", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class InformationNote
    {
        [XmlElement(ElementName = "Subject", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string Subject { get; set; }
        [XmlElement(ElementName = "Content", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string Content { get; set; }
    }

    [XmlRoot(ElementName = "SpecifiedTradeProduct", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class SpecifiedTradeProduct
    {
        [XmlElement(ElementName = "ID", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string ID { get; set; }
        [XmlElement(ElementName = "Name", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string Name { get; set; }
        [XmlElement(ElementName = "Description", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string Description { get; set; }
        [XmlElement(ElementName = "InformationNote", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public InformationNote InformationNote { get; set; }
    }

    [XmlRoot(ElementName = "ChargeAmount", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class ChargeAmount
    {
        [XmlAttribute(AttributeName = "currencyCode")]
        public string CurrencyCode { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "GrossPriceProductTradePrice", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class GrossPriceProductTradePrice
    {
        [XmlElement(ElementName = "ChargeAmount", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public ChargeAmount ChargeAmount { get; set; }
    }

    [XmlRoot(ElementName = "SpecifiedLineTradeAgreement", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class SpecifiedLineTradeAgreement
    {
        [XmlElement(ElementName = "GrossPriceProductTradePrice", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public GrossPriceProductTradePrice GrossPriceProductTradePrice { get; set; }
    }

    [XmlRoot(ElementName = "BilledQuantity", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class BilledQuantity
    {
        [XmlAttribute(AttributeName = "unitCode")]
        public string UnitCode { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "PerPackageUnitQuantity", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class PerPackageUnitQuantity
    {
        [XmlAttribute(AttributeName = "unitCode")]
        public string UnitCode { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "SpecifiedLineTradeDelivery", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class SpecifiedLineTradeDelivery
    {
        [XmlElement(ElementName = "BilledQuantity", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public BilledQuantity BilledQuantity { get; set; }
        [XmlElement(ElementName = "PerPackageUnitQuantity", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public PerPackageUnitQuantity PerPackageUnitQuantity { get; set; }
    }

    [XmlRoot(ElementName = "LineTotalAmount", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class LineTotalAmount
    {
        [XmlAttribute(AttributeName = "currencyCode")]
        public string CurrencyCode { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "SpecifiedTradeSettlementLineMonetarySummation", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class SpecifiedTradeSettlementLineMonetarySummation
    {
        [XmlElement(ElementName = "LineTotalAmount", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public LineTotalAmount LineTotalAmount { get; set; }
    }

    [XmlRoot(ElementName = "SpecifiedLineTradeSettlement", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class SpecifiedLineTradeSettlement
    {
        [XmlElement(ElementName = "SpecifiedTradeSettlementLineMonetarySummation", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public SpecifiedTradeSettlementLineMonetarySummation SpecifiedTradeSettlementLineMonetarySummation { get; set; }
    }

    [XmlRoot(ElementName = "IncludedSupplyChainTradeLineItem", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class IncludedSupplyChainTradeLineItem
    {
        [XmlElement(ElementName = "AssociatedDocumentLineDocument", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public AssociatedDocumentLineDocument AssociatedDocumentLineDocument { get; set; }
        [XmlElement(ElementName = "SpecifiedTradeProduct", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public SpecifiedTradeProduct SpecifiedTradeProduct { get; set; }
        [XmlElement(ElementName = "SpecifiedLineTradeAgreement", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public SpecifiedLineTradeAgreement SpecifiedLineTradeAgreement { get; set; }
        [XmlElement(ElementName = "SpecifiedLineTradeDelivery", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public SpecifiedLineTradeDelivery SpecifiedLineTradeDelivery { get; set; }
        [XmlElement(ElementName = "SpecifiedLineTradeSettlement", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public SpecifiedLineTradeSettlement SpecifiedLineTradeSettlement { get; set; }
    }

    [XmlRoot(ElementName = "AdditionalReferencedDocument", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class AdditionalReferencedDocument
    {
        [XmlElement(ElementName = "IssuerAssignedID", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string IssuerAssignedID { get; set; }
        [XmlElement(ElementName = "TypeCode", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string TypeCode { get; set; }
        [XmlElement(ElementName = "Name", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string Name { get; set; }
    }

    [XmlRoot(ElementName = "ApplicableHeaderTradeAgreement", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class ApplicableHeaderTradeAgreement
    {
        [XmlElement(ElementName = "AdditionalReferencedDocument", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public AdditionalReferencedDocument AdditionalReferencedDocument { get; set; }
    }

    [XmlRoot(ElementName = "DefinedTradeContact", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class DefinedTradeContact
    {
        [XmlElement(ElementName = "PersonName", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string PersonName { get; set; }
        [XmlElement(ElementName = "DepartmentName", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string DepartmentName { get; set; }
        [XmlElement(ElementName = "TelephoneUniversalCommunication", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string TelephoneUniversalCommunication { get; set; }
        [XmlElement(ElementName = "EmailURIUniversalCommunication", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string EmailURIUniversalCommunication { get; set; }
    }

    [XmlRoot(ElementName = "PostalTradeAddress", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class PostalTradeAddress
    {
        [XmlElement(ElementName = "PostcodeCode", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string PostcodeCode { get; set; }
        [XmlElement(ElementName = "BuildingName", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string BuildingName { get; set; }
        [XmlElement(ElementName = "LineOne", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string LineOne { get; set; }
        [XmlElement(ElementName = "LineTwo", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string LineTwo { get; set; }
        [XmlElement(ElementName = "LineThree", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string LineThree { get; set; }
        [XmlElement(ElementName = "LineFour", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string LineFour { get; set; }
        [XmlElement(ElementName = "LineFive", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string LineFive { get; set; }
        [XmlElement(ElementName = "StreetName", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string StreetName { get; set; }
        [XmlElement(ElementName = "CityName", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string CityName { get; set; }
        [XmlElement(ElementName = "CitySubDivisionName", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string CitySubDivisionName { get; set; }
        [XmlElement(ElementName = "CountryID", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string CountryID { get; set; }
        [XmlElement(ElementName = "CountrySubDivisionID", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string CountrySubDivisionID { get; set; }
        [XmlElement(ElementName = "BuildingNumber", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string BuildingNumber { get; set; }
    }

    [XmlRoot(ElementName = "SpecifiedTaxRegistration", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class SpecifiedTaxRegistration
    {
        [XmlElement(ElementName = "ID", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public ID ID { get; set; }
    }

    [XmlRoot(ElementName = "PayeeTradeParty", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class PayeeTradeParty
    {
        [XmlElement(ElementName = "ID", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string ID { get; set; }
        [XmlElement(ElementName = "Name", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string Name { get; set; }
        [XmlElement(ElementName = "DefinedTradeContact", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public DefinedTradeContact DefinedTradeContact { get; set; }
        [XmlElement(ElementName = "PostalTradeAddress", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public PostalTradeAddress PostalTradeAddress { get; set; }
        [XmlElement(ElementName = "SpecifiedTaxRegistration", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public SpecifiedTaxRegistration SpecifiedTaxRegistration { get; set; }
    }

    [XmlRoot(ElementName = "PayerTradeParty", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class PayerTradeParty
    {
        [XmlElement(ElementName = "ID", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string ID { get; set; }
        [XmlElement(ElementName = "Name", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string Name { get; set; }
        [XmlElement(ElementName = "DefinedTradeContact", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public DefinedTradeContact DefinedTradeContact { get; set; }
        [XmlElement(ElementName = "PostalTradeAddress", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public PostalTradeAddress PostalTradeAddress { get; set; }
        [XmlElement(ElementName = "SpecifiedTaxRegistration", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public SpecifiedTaxRegistration SpecifiedTaxRegistration { get; set; }
    }

    [XmlRoot(ElementName = "SpecifiedTradeSettlementPaymentMeans", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class SpecifiedTradeSettlementPaymentMeans
    {
        [XmlElement(ElementName = "TypeCode", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string TypeCode { get; set; }
        [XmlElement(ElementName = "Information", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string Information { get; set; }
    }

    [XmlRoot(ElementName = "CalculatedAmount", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class CalculatedAmount
    {
        [XmlAttribute(AttributeName = "currencyCode")]
        public string CurrencyCode { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "BasisAmount", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class BasisAmount
    {
        [XmlAttribute(AttributeName = "currencyCode")]
        public string CurrencyCode { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "ApplicableTradeTax", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class ApplicableTradeTax
    {
        [XmlElement(ElementName = "CalculatedAmount", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public CalculatedAmount CalculatedAmount { get; set; }
        [XmlElement(ElementName = "TypeCode", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string TypeCode { get; set; }
        [XmlElement(ElementName = "CalculatedRate", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string CalculatedRate { get; set; }
        [XmlElement(ElementName = "BasisAmount", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public BasisAmount BasisAmount { get; set; }
    }

    [XmlRoot(ElementName = "ActualAmount", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class ActualAmount
    {
        [XmlAttribute(AttributeName = "currencyCode")]
        public string CurrencyCode { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "SpecifiedTradeAllowanceCharge", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class SpecifiedTradeAllowanceCharge
    {
        [XmlElement(ElementName = "ChargeIndicator", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string ChargeIndicator { get; set; }
        [XmlElement(ElementName = "ActualAmount", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public ActualAmount ActualAmount { get; set; }
        [XmlElement(ElementName = "ReasonCode", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string ReasonCode { get; set; }
        [XmlElement(ElementName = "Reason", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string Reason { get; set; }
        [XmlElement(ElementName = "TypeCode", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string TypeCode { get; set; }
    }

    [XmlRoot(ElementName = "ChargeTotalAmount", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class ChargeTotalAmount
    {
        [XmlAttribute(AttributeName = "currencyCode")]
        public string CurrencyCode { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "AllowanceTotalAmount", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class AllowanceTotalAmount
    {
        [XmlAttribute(AttributeName = "currencyCode")]
        public string CurrencyCode { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "TaxBasisTotalAmount", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class TaxBasisTotalAmount
    {
        [XmlAttribute(AttributeName = "currencyCode")]
        public string CurrencyCode { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "TaxTotalAmount", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class TaxTotalAmount
    {
        [XmlAttribute(AttributeName = "currencyCode")]
        public string CurrencyCode { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "GrandTotalAmount", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class GrandTotalAmount
    {
        [XmlAttribute(AttributeName = "currencyCode")]
        public string CurrencyCode { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "SpecifiedTradeSettlementHeaderMonetarySummation", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class SpecifiedTradeSettlementHeaderMonetarySummation
    {
        [XmlElement(ElementName = "LineTotalAmount", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public LineTotalAmount LineTotalAmount { get; set; }
        [XmlElement(ElementName = "ChargeTotalAmount", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public ChargeTotalAmount ChargeTotalAmount { get; set; }
        [XmlElement(ElementName = "AllowanceTotalAmount", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public AllowanceTotalAmount AllowanceTotalAmount { get; set; }
        [XmlElement(ElementName = "TaxBasisTotalAmount", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public TaxBasisTotalAmount TaxBasisTotalAmount { get; set; }
        [XmlElement(ElementName = "TaxTotalAmount", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public TaxTotalAmount TaxTotalAmount { get; set; }
        [XmlElement(ElementName = "GrandTotalAmount", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public GrandTotalAmount GrandTotalAmount { get; set; }
    }

    [XmlRoot(ElementName = "ApplicableHeaderTradeSettlement", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
    public class ApplicableHeaderTradeSettlement
    {
        [XmlElement(ElementName = "PayeeTradeParty", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public PayeeTradeParty PayeeTradeParty { get; set; }
        [XmlElement(ElementName = "PayerTradeParty", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public PayerTradeParty PayerTradeParty { get; set; }
        [XmlElement(ElementName = "InvoiceCurrencyCode", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public string InvoiceCurrencyCode { get; set; }
        [XmlElement(ElementName = "SpecifiedTradeSettlementPaymentMeans", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public SpecifiedTradeSettlementPaymentMeans SpecifiedTradeSettlementPaymentMeans { get; set; }
        [XmlElement(ElementName = "ApplicableTradeTax", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public ApplicableTradeTax ApplicableTradeTax { get; set; }
        [XmlElement(ElementName = "SpecifiedTradeAllowanceCharge", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public SpecifiedTradeAllowanceCharge SpecifiedTradeAllowanceCharge { get; set; }
        [XmlElement(ElementName = "SpecifiedTradeSettlementHeaderMonetarySummation", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public SpecifiedTradeSettlementHeaderMonetarySummation SpecifiedTradeSettlementHeaderMonetarySummation { get; set; }
    }

    [XmlRoot(ElementName = "SupplyChainTradeTransaction", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt:2")]
    public class SupplyChainTradeTransaction
    {
        [XmlElement(ElementName = "IncludedSupplyChainTradeLineItem", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public IncludedSupplyChainTradeLineItem IncludedSupplyChainTradeLineItem { get; set; }
        [XmlElement(ElementName = "ApplicableHeaderTradeAgreement", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public ApplicableHeaderTradeAgreement ApplicableHeaderTradeAgreement { get; set; }
        [XmlElement(ElementName = "ApplicableHeaderTradeSettlement", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt_ReusableAggregateBusinessInformationEntity:2")]
        public ApplicableHeaderTradeSettlement ApplicableHeaderTradeSettlement { get; set; }
    }

    [XmlRoot(ElementName = "CanonicalizationMethod", Namespace = "http://www.w3.org/2000/09/xmldsig#")]
    public class CanonicalizationMethod
    {
        [XmlAttribute(AttributeName = "Algorithm")]
        public string Algorithm { get; set; }
    }

    [XmlRoot(ElementName = "SignatureMethod", Namespace = "http://www.w3.org/2000/09/xmldsig#")]
    public class SignatureMethod
    {
        [XmlElement(ElementName = "HMACOutputLength", Namespace = "http://www.w3.org/2000/09/xmldsig#")]
        public string HMACOutputLength { get; set; }
        [XmlAttribute(AttributeName = "Algorithm")]
        public string Algorithm { get; set; }
    }

    [XmlRoot(ElementName = "Transform", Namespace = "http://www.w3.org/2000/09/xmldsig#")]
    public class Transform
    {
        [XmlAttribute(AttributeName = "Algorithm")]
        public string Algorithm { get; set; }
    }

    [XmlRoot(ElementName = "Transforms", Namespace = "http://www.w3.org/2000/09/xmldsig#")]
    public class Transforms
    {
        [XmlElement(ElementName = "Transform", Namespace = "http://www.w3.org/2000/09/xmldsig#")]
        public Transform Transform { get; set; }
    }

    [XmlRoot(ElementName = "DigestMethod", Namespace = "http://www.w3.org/2000/09/xmldsig#")]
    public class DigestMethod
    {
        [XmlAttribute(AttributeName = "Algorithm")]
        public string Algorithm { get; set; }
    }

    [XmlRoot(ElementName = "Reference", Namespace = "http://www.w3.org/2000/09/xmldsig#")]
    public class Reference
    {
        [XmlElement(ElementName = "Transforms", Namespace = "http://www.w3.org/2000/09/xmldsig#")]
        public Transforms Transforms { get; set; }
        [XmlElement(ElementName = "DigestMethod", Namespace = "http://www.w3.org/2000/09/xmldsig#")]
        public DigestMethod DigestMethod { get; set; }
        [XmlElement(ElementName = "DigestValue", Namespace = "http://www.w3.org/2000/09/xmldsig#")]
        public string DigestValue { get; set; }
        [XmlAttribute(AttributeName = "Id")]
        public string Id { get; set; }
        [XmlAttribute(AttributeName = "URI")]
        public string URI { get; set; }
        [XmlAttribute(AttributeName = "Type")]
        public string Type { get; set; }
    }

    [XmlRoot(ElementName = "SignedInfo", Namespace = "http://www.w3.org/2000/09/xmldsig#")]
    public class SignedInfo
    {
        [XmlElement(ElementName = "CanonicalizationMethod", Namespace = "http://www.w3.org/2000/09/xmldsig#")]
        public CanonicalizationMethod CanonicalizationMethod { get; set; }
        [XmlElement(ElementName = "SignatureMethod", Namespace = "http://www.w3.org/2000/09/xmldsig#")]
        public SignatureMethod SignatureMethod { get; set; }
        [XmlElement(ElementName = "Reference", Namespace = "http://www.w3.org/2000/09/xmldsig#")]
        public Reference Reference { get; set; }
        [XmlAttribute(AttributeName = "Id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "SignatureValue", Namespace = "http://www.w3.org/2000/09/xmldsig#")]
    public class SignatureValue
    {
        [XmlAttribute(AttributeName = "Id")]
        public string Id { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "KeyInfo", Namespace = "http://www.w3.org/2000/09/xmldsig#")]
    public class KeyInfo
    {
        [XmlElement(ElementName = "KeyName", Namespace = "http://www.w3.org/2000/09/xmldsig#")]
        public string KeyName { get; set; }
        [XmlAttribute(AttributeName = "Id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "Object", Namespace = "http://www.w3.org/2000/09/xmldsig#")]
    public class Object
    {
        [XmlAttribute(AttributeName = "Id")]
        public string Id { get; set; }
        [XmlAttribute(AttributeName = "MimeType")]
        public string MimeType { get; set; }
        [XmlAttribute(AttributeName = "Encoding")]
        public string Encoding { get; set; }
    }

    [XmlRoot(ElementName = "Signature", Namespace = "http://www.w3.org/2000/09/xmldsig#")]
    public class Signature
    {
        [XmlElement(ElementName = "SignedInfo", Namespace = "http://www.w3.org/2000/09/xmldsig#")]
        public SignedInfo SignedInfo { get; set; }
        [XmlElement(ElementName = "SignatureValue", Namespace = "http://www.w3.org/2000/09/xmldsig#")]
        public SignatureValue SignatureValue { get; set; }
        [XmlElement(ElementName = "KeyInfo", Namespace = "http://www.w3.org/2000/09/xmldsig#")]
        public KeyInfo KeyInfo { get; set; }
        [XmlElement(ElementName = "Object", Namespace = "http://www.w3.org/2000/09/xmldsig#")]
        public Object Object { get; set; }
        [XmlAttribute(AttributeName = "Id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "GovernmentReceipt", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt:2")]
    public class GovernmentReceipt
    {
        [XmlElement(ElementName = "ExchangedDocumentContext", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt:2")]
        public ExchangedDocumentContext ExchangedDocumentContext { get; set; }
        [XmlElement(ElementName = "ExchangedDocument", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt:2")]
        public ExchangedDocument ExchangedDocument { get; set; }
        [XmlElement(ElementName = "SupplyChainTradeTransaction", Namespace = "urn:etda:unece:uncefact:data:standard:GovernmentReceipt:2")]
        public SupplyChainTradeTransaction SupplyChainTradeTransaction { get; set; }
        [XmlElement(ElementName = "Signature", Namespace = "http://www.w3.org/2000/09/xmldsig#")]
        public Signature Signature { get; set; }
        [XmlAttribute(AttributeName = "udt", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Udt { get; set; }
        [XmlAttribute(AttributeName = "ram", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Ram { get; set; }
        [XmlAttribute(AttributeName = "ccts", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Ccts { get; set; }
        [XmlAttribute(AttributeName = "qdt", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Qdt { get; set; }
        [XmlAttribute(AttributeName = "rsm", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Rsm { get; set; }
        [XmlAttribute(AttributeName = "ds", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Ds { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }
        [XmlAttribute(AttributeName = "schemaLocation", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string SchemaLocation { get; set; }
    }

}
